<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt">
<context>
    <name>CacheSettings</name>
    <message>
        <source>Manage cache</source>
        <translation>Gerir cache</translation>
    </message>
</context>
<context>
    <name>CacheSettingsPage</name>
    <message>
        <source>Manage cache</source>
        <translation>Gerir cache</translation>
    </message>
    <message>
        <source>One Week</source>
        <translation>Uma semana</translation>
    </message>
    <message>
        <source>Fortnight</source>
        <translation>Quinzena</translation>
    </message>
    <message>
        <source>One Month</source>
        <translation>Um mês</translation>
    </message>
    <message>
        <source>Three Months</source>
        <translation>Três meses</translation>
    </message>
    <message>
        <source>Six Months</source>
        <translation>Seis meses</translation>
    </message>
    <message>
        <source>Clear messages from cache</source>
        <translation>Limpar mensagens da cache</translation>
    </message>
    <message>
        <source>This will clear messages older than the given period</source>
        <translation>Isto limpará as mensagens anteriores ao período especificado</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Limpar</translation>
    </message>
</context>
</TS>
