<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl">
<context>
    <name>CacheSettings</name>
    <message>
        <source>Manage cache</source>
        <translation>Zarządzaj pamięcią podręczną</translation>
    </message>
</context>
<context>
    <name>CacheSettingsPage</name>
    <message>
        <source>Manage cache</source>
        <translation>Zarządzaj pamięcią podręczną</translation>
    </message>
    <message>
        <source>One Week</source>
        <translation>1 tydzień</translation>
    </message>
    <message>
        <source>Fortnight</source>
        <translation>2 tygodnie</translation>
    </message>
    <message>
        <source>One Month</source>
        <translation>1 miesiąc</translation>
    </message>
    <message>
        <source>Three Months</source>
        <translation>3 miesiące</translation>
    </message>
    <message>
        <source>Six Months</source>
        <translation>6 miesięcy</translation>
    </message>
    <message>
        <source>Clear messages from cache</source>
        <translation>Wyczyść wiadomości z pamięci podręcznej</translation>
    </message>
    <message>
        <source>This will clear messages older than the given period</source>
        <translation>Spowoduje to wyczyszczenie wiadomości starszych niż podany okres</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Wyczyść</translation>
    </message>
</context>
</TS>
