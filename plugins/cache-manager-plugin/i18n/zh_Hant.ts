<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>CacheSettings</name>
    <message>
        <source>Manage cache</source>
        <translation>管理快取</translation>
    </message>
</context>
<context>
    <name>CacheSettingsPage</name>
    <message>
        <source>Manage cache</source>
        <translation>管理快取</translation>
    </message>
    <message>
        <source>One Week</source>
        <translation>一星期</translation>
    </message>
    <message>
        <source>Fortnight</source>
        <translation>兩星期</translation>
    </message>
    <message>
        <source>One Month</source>
        <translation>一個月</translation>
    </message>
    <message>
        <source>Three Months</source>
        <translation>三個月</translation>
    </message>
    <message>
        <source>Six Months</source>
        <translation>六個月</translation>
    </message>
    <message>
        <source>Clear messages from cache</source>
        <translation>清理快取中的訊息</translation>
    </message>
    <message>
        <source>This will clear messages older than the given period</source>
        <translation>這將會清除早於給定時間內的郵件</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>清理</translation>
    </message>
</context>
</TS>
