<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name>CacheSettings</name>
    <message>
        <source>Manage cache</source>
        <translation>Hantera cache</translation>
    </message>
</context>
<context>
    <name>CacheSettingsPage</name>
    <message>
        <source>Manage cache</source>
        <translation>Hantera cache</translation>
    </message>
    <message>
        <source>One Week</source>
        <translation>En Vecka</translation>
    </message>
    <message>
        <source>Fortnight</source>
        <translation>Fjorton dagar</translation>
    </message>
    <message>
        <source>One Month</source>
        <translation>En Månad</translation>
    </message>
    <message>
        <source>Three Months</source>
        <translation>Tre Månader</translation>
    </message>
    <message>
        <source>Six Months</source>
        <translation>Sex Månader</translation>
    </message>
    <message>
        <source>Clear messages from cache</source>
        <translation>Rensa meddelande från cache</translation>
    </message>
    <message>
        <source>This will clear messages older than the given period</source>
        <translation>Detta rensar meddelanden som är äldre än den angivna perioden</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Rensa</translation>
    </message>
</context>
</TS>
