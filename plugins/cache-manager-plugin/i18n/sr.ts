<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sr">
<context>
    <name>CacheSettings</name>
    <message>
        <source>Manage cache</source>
        <translation>Управљање кешом</translation>
    </message>
</context>
<context>
    <name>CacheSettingsPage</name>
    <message>
        <source>Manage cache</source>
        <translation>Управљање кешом</translation>
    </message>
    <message>
        <source>One Week</source>
        <translation>Једна недеља</translation>
    </message>
    <message>
        <source>Fortnight</source>
        <translation>Две недеље</translation>
    </message>
    <message>
        <source>One Month</source>
        <translation>Један месец</translation>
    </message>
    <message>
        <source>Three Months</source>
        <translation>Три месеца</translation>
    </message>
    <message>
        <source>Six Months</source>
        <translation>Шест месеци</translation>
    </message>
    <message>
        <source>Clear messages from cache</source>
        <translation>Обриши поруке из кеша</translation>
    </message>
    <message>
        <source>This will clear messages older than the given period</source>
        <translation>Ово ће обрисати поруке старије од датог периода</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Обриши</translation>
    </message>
</context>
</TS>
