<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_GB">
<context>
    <name>CacheSettings</name>
    <message>
        <source>Manage cache</source>
        <translation>Cache verwalten</translation>
    </message>
</context>
<context>
    <name>CacheSettingsPage</name>
    <message>
        <source>Manage cache</source>
        <translation>Cache verwalten</translation>
    </message>
    <message>
        <source>One Week</source>
        <translation>Eine Woche</translation>
    </message>
    <message>
        <source>Fortnight</source>
        <translation>Vierzehn Tage</translation>
    </message>
    <message>
        <source>One Month</source>
        <translation>Ein Monat</translation>
    </message>
    <message>
        <source>Three Months</source>
        <translation>Drei Monate</translation>
    </message>
    <message>
        <source>Six Months</source>
        <translation>Sechs Monate</translation>
    </message>
    <message>
        <source>Clear messages from cache</source>
        <translation>Nachrichten aus Cache entfernen</translation>
    </message>
    <message>
        <source>This will clear messages older than the given period</source>
        <translation>Dieses wird Nachrichten älter als der eingestellte Zeitraum löschen</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Löschen</translation>
    </message>
</context>
</TS>
