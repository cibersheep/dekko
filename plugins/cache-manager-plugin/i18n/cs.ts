<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>CacheSettings</name>
    <message>
        <source>Manage cache</source>
        <translation>Správa mezipaměti</translation>
    </message>
</context>
<context>
    <name>CacheSettingsPage</name>
    <message>
        <source>Manage cache</source>
        <translation>Správa mezipaměti</translation>
    </message>
    <message>
        <source>One Week</source>
        <translation>Jeden týden</translation>
    </message>
    <message>
        <source>Fortnight</source>
        <translation>Čtrnáct dní</translation>
    </message>
    <message>
        <source>One Month</source>
        <translation>Jeden měsíc</translation>
    </message>
    <message>
        <source>Three Months</source>
        <translation>Tři týdny</translation>
    </message>
    <message>
        <source>Six Months</source>
        <translation>Šest měsíců</translation>
    </message>
    <message>
        <source>Clear messages from cache</source>
        <translation>Vymazat zprávy z mezipaměti</translation>
    </message>
    <message>
        <source>This will clear messages older than the given period</source>
        <translation>Tímto vymažete zprávy starší než dané období</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Vymazat</translation>
    </message>
</context>
</TS>
