<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nb_NO">
<context>
    <name>CacheSettings</name>
    <message>
        <source>Manage cache</source>
        <translation>Behandle cache</translation>
    </message>
</context>
<context>
    <name>CacheSettingsPage</name>
    <message>
        <source>Manage cache</source>
        <translation>Behandle cache</translation>
    </message>
    <message>
        <source>One Week</source>
        <translation>En uke</translation>
    </message>
    <message>
        <source>Fortnight</source>
        <translation>Fjorten dager</translation>
    </message>
    <message>
        <source>One Month</source>
        <translation>En måned</translation>
    </message>
    <message>
        <source>Three Months</source>
        <translation>Tre måneder</translation>
    </message>
    <message>
        <source>Six Months</source>
        <translation>Seks måneder</translation>
    </message>
    <message>
        <source>Clear messages from cache</source>
        <translation>Slett meldinger i cache</translation>
    </message>
    <message>
        <source>This will clear messages older than the given period</source>
        <translation>Dette vil slette meldinger som er eldre enn angii periode</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Slett</translation>
    </message>
</context>
</TS>
