<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sr">
<context>
    <name>NotificationSettings</name>
    <message>
        <source>Notifications</source>
        <translation>Обавештења</translation>
    </message>
</context>
<context>
    <name>NotificationSettingsPage</name>
    <message>
        <source>Notification Settings</source>
        <translation>Подешавања за обавештења</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation>Укључено</translation>
    </message>
</context>
</TS>
