<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name>NotificationSettings</name>
    <message>
        <source>Notifications</source>
        <translation>Meddelanden</translation>
    </message>
</context>
<context>
    <name>NotificationSettingsPage</name>
    <message>
        <source>Notification Settings</source>
        <translation>Meddelanden Inställningar</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation>Aktiverad</translation>
    </message>
</context>
</TS>
