<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt">
<context>
    <name>NotificationSettings</name>
    <message>
        <source>Notifications</source>
        <translation>Notificações</translation>
    </message>
</context>
<context>
    <name>NotificationSettingsPage</name>
    <message>
        <source>Notification Settings</source>
        <translation>Definições das notificações</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation>Ativado</translation>
    </message>
</context>
</TS>
