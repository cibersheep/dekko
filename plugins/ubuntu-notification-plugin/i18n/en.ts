<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_GB">
<context>
    <name>NotificationSettings</name>
    <message>
        <source>Notifications</source>
        <translation>Benachrichtigungen</translation>
    </message>
</context>
<context>
    <name>NotificationSettingsPage</name>
    <message>
        <source>Notification Settings</source>
        <translation>Benachrichtigungseinstellungen</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation>Aktiviert</translation>
    </message>
</context>
</TS>
