<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nb_NO">
<context>
    <name>NotificationSettings</name>
    <message>
        <source>Notifications</source>
        <translation>Varsler</translation>
    </message>
</context>
<context>
    <name>NotificationSettingsPage</name>
    <message>
        <source>Notification Settings</source>
        <translation>Varslingsinnstillinger</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation>Aktivert</translation>
    </message>
</context>
</TS>
